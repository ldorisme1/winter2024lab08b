import java.util.Random;
public class Board {
	private Tile[][] grid;
	private final int size = 5;// the size never changes so we can set it as a constant so everywhere using
								// size will be 5

	public Board() {
		this.grid = new Tile[size][size];// should I initialize a new grid array foreach method?
		Random rand = new Random();
		for (int i = 0; i < size; i++) {
				int randPosition = rand.nextInt(this.grid[i].length);
			for (int j = 0; j < size; j++) {
				this.grid[i][j] = Tile.BLANK;
				this.grid[i][randPosition] = Tile.HIDDEN_WALL;
			}
		}
	}

	public String toString() {
		String myBoard = "";
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				myBoard += this.grid[i][j].getName() + " ";
			}
			myBoard += "\n";
		}
		return myBoard;
	}

	public int placeToken(int row, int col) {
		int indication =0;
		// should I put an error message here for the row and col
		if(row < 0 || row >= size || col < 0 || col >= size) {
			indication =-2;
		}
		else if(this.grid[row][col].equals(Tile.CASTLE) || this.grid[row][col].equals(Tile.WALL)){
				indication =-1;
		}
		else if(this.grid[row][col].equals(Tile.HIDDEN_WALL)){
			indication=1;
			this.grid[row][col] = Tile.WALL;
		}
		else if(this.grid[row][col].equals(Tile.BLANK)){
			indication=0;
			this.grid[row][col] = Tile.CASTLE;
		}
			
		return indication;

	}

}
